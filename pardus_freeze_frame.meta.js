// ==UserScript==
// @name        Pardus Freeze Frame
// @namespace   http://userscripts.xcom-alliance.info/
// @author      Miche (Orion) / Sparkle (Artemis)
// @version     1.1
// @description Prevents you moving off your current tile via keypress or mouse click when the commands icon shows as being locked
// @include     http*://*.pardus.at/main.php
// @updateURL   http://userscripts.xcom-alliance.info/freeze_frame/pardus_freeze_frame.meta.js
// @downloadURL http://userscripts.xcom-alliance.info/freeze_frame/pardus_freeze_frame.user.js
// @icon        http://userscripts.xcom-alliance.info/freeze_frame/icon.png
// @grant       none
// ==/UserScript==
