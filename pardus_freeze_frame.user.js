// ==UserScript==
// @name        Pardus Freeze Frame
// @namespace   http://userscripts.xcom-alliance.info/
// @author      Miche (Orion) / Sparkle (Artemis)
// @version     1.1
// @description Prevents you moving off your current tile via keypress or mouse click when the commands icon shows as being locked
// @include     http*://*.pardus.at/main.php*
// @updateURL   http://userscripts.xcom-alliance.info/freeze_frame/pardus_freeze_frame.meta.js
// @downloadURL http://userscripts.xcom-alliance.info/freeze_frame/pardus_freeze_frame.user.js
// @icon        http://userscripts.xcom-alliance.info/freeze_frame/icon.png
// @grant       none
// ==/UserScript==

function getUniverse() { return window.location.host.substr(0,window.location.host.indexOf('.')).toLowerCase() };

function LockState(img) {
	// image created at http://dopiaza.org/tools/datauri/index.php
	var lockedImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAMAAAAoLQ9TAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAN5QTFRFAAAdAAAc////5eXo2dndj4+clpaixMTKn5+q8/P0xMTL19fcwcHIXl5wRERZt7e/iYmW+fn6ODhPOTlPNzdOGho0LS1EBQUhnp6pQkJXm5umU1NnKipCk5OfqqqzOTlQe3uKKytDmJik19fbKChAeXmId3eGY2N17+/xIyM80NDVmpql39/j/Pz8LS1F5ubpBgYi/f39BgYjUFBjoKCqBQUjPz9Vf3+Oj4+bCwsnpaWvh4eVr6+4iIiWGhozNjZM2NjcFRUw6+vtycnPTk5iurrC4eHkWVlrOztRgYGPUa4ZRQAAAAF0Uk5TAEDm2GYAAACGSURBVHjaYmAAAnUbOWMGJODKxM/DJIXgewoCCWkmXriAhhqIlHCDCzCBSRcmGN8MwpKEC/AJeIApa3kI34GJ05AJCHQ5mUzAAtxMcMABFlCVZQUDC1Yld7AAjxMzFHCyggWU0bWYa7NBAQfErfw6LFCgb4vdFitndi52di4g1lIA8QECDADe7Qk5ZwZZdgAAAABJRU5ErkJggg==";
	img.src = lockedImage;
	img.setAttribute('title','Click to allow normal movement');
	img.setAttribute('id','useform');
	var els = document.querySelectorAll('#tdSpaceChart table td a');
	for (var i=0; i<els.length; i++) {
		els[i].setAttribute('_onclick', els[i].getAttribute('onclick'));
		els[i].removeAttribute('onclick');
	}
};

function UnlockState(img) {
	// image created at http://dopiaza.org/tools/datauri/index.php
	var unlockedImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAMAAAAoLQ9TAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAIdQTFRFAAAd////AAAc2dndBQUj5eXoBgYixMTK4eHkFRUw+fn6xMTL19fc/f398/P0LS1ENjZMKChABQUhIyM8BgYj4eHl0NDVnZ2ot7e/6+vtycnP2Njcw8POLS1FODhP39/j19fbCwsnOTlPn5+qwcHIGho0KipCGhozKytD4eHjNzdO5ubppaWvXLIcpwAAAAF0Uk5TAEDm2GYAAABjSURBVHjaYmDACcQZGTnE0fiMjAgRcS4usCBCgA8kqYkQkAEzRRECHIwwUpqRj5cRCHi5oLI8jHAANQ0BIAISmszyzGDEBxFQwK1FGapFTAEKWHFo0ZFi52Zn5wZiMTAfIMAAnukGs8EzFuUAAAAASUVORK5CYII=";
	img.src = unlockedImage;
	img.setAttribute('title','Click to prevent normal movement');
	if (img.hasAttribute('id')) img.removeAttribute('id');
	var els = document.querySelectorAll('#tdSpaceChart table td a');
	for (var i=0; i<els.length; i++) {
		if (els[i].hasAttribute('_onclick')) {
			els[i].setAttribute('onclick', els[i].getAttribute('_onclick'));
			els[i].removeAttribute('_onclick');
		}
	}
};

function ToggleState(img) {
	if (localStorage.getItem(getUniverse() + '_lockstate') == 'locked') {
		UnlockState(img);
		localStorage.setItem(getUniverse() + '_lockstate', 'unlocked');
	} else {
		LockState(img);
		localStorage.setItem(getUniverse() + '_lockstate', 'locked');
	}
};

var insert = document.getElementById('commands_image');
if (insert) {
	var img = document.createElement('img');
	img.setAttribute('style','position:absolute;z-index:2;margin:6px 0 0 -31px');
	insert.parentNode.appendChild(img);
	if (localStorage.getItem(getUniverse() + '_lockstate') == 'locked') {
		LockState(img);
	} else {
		UnlockState(img);
	}
	img.addEventListener('click', function(evt) { ToggleState(evt.target) }, true);
}